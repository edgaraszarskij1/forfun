export interface TodoList {
  id: string;
  isFinished: boolean;
  description: string;
}
