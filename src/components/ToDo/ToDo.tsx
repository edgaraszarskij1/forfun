import React, { useState, useReducer } from "react";
import { reducer, initialState } from "../../reducers/ToDoReducer";
import "../../App.css";

export const ToDo = (): JSX.Element => {
  const [state, dispatch] = useReducer(reducer, initialState);
  const [todoItem, setTodoItem] = useState<string>("");

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setTodoItem(event.target.value);
  };

  const onClick = (todo: string) => {
    dispatch({ type: "ADD", value: todo });
    setTodoItem("");
  };

  const isCompleted = (id: string) => {
    dispatch({ type: "FINISHED", id: id });
  };

  const onClickDelete = (id: string) => {
    dispatch({ type: "DELETE", id: id });
  };

  return (
    <div className="container">
      {state &&
        state.map((todo) => (
          <div key={todo.id} className="todo">
            <div
              onClick={() => isCompleted(todo.id)}
              className={todo.isFinished ? "text-through" : "text"}
            >
              {todo.description}
            </div>
            <div onClick={() => onClickDelete(todo.id)} className={"child"}>
              X
            </div>
          </div>
        ))}
      <input
        name="todoItem"
        value={todoItem}
        onChange={handleChange}
        className="text-field"
      />
      <button onClick={() => onClick(todoItem)} className="button">
        Add
      </button>
    </div>
  );
};
