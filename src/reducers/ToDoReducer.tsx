import { TodoList } from "../interfaces/TodoListInterface";
import { nanoid } from "nanoid";

export const initialState: TodoList[] = JSON.parse(
  localStorage.getItem("ToDo") || "[]"
);

type ACTIONTYPE =
  | { type: "ADD"; value: string }
  | { type: "FINISHED"; id: string }
  | { type: "DELETE"; id: string };

export const reducer = (state: typeof initialState, action: ACTIONTYPE) => {
  switch (action.type) {
    case "ADD":
      const newItem = [
        ...state,
        { id: nanoid(), isFinished: false, description: action.value },
      ];
      window.localStorage.setItem("ToDo", JSON.stringify(newItem));
      return newItem;

    case "FINISHED":
      const newState = state.map((item) =>
        item.id === action.id ? { ...item, isFinished: !item.isFinished } : item
      );
      window.localStorage.setItem("ToDo", JSON.stringify(newState));

      return newState;

    case "DELETE":
      const todoList = state.filter((todo) => todo.id !== action.id);
      window.localStorage.setItem("ToDo", JSON.stringify(todoList));
      return todoList;
  }
};
