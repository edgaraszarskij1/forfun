import React from "react";
import { ToDo } from "./components/ToDo/ToDo";

function App() {
  return <ToDo />;
}

export default App;
